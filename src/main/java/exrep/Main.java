package exrep;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import otherpackage.Util;

public class Main {

	@Test
	public void ex1() {
		new Company();
		new Company();
	}

	@Test
	public void ex2() {
		try {
			System.out.println("in try block");
			// throw new RuntimeException();
			// return;

		} catch (Exception e) {
			System.out.println("Catched: " + e);
		} finally {
			System.out.println("in finally block");
		}
	}

	@Test
	public void ex3() {
		try {
			Util.readFromFile();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		try {
			Util.readFromWeb();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void ex4() {
		List<Person> persons1 = Arrays.asList(new Person("Jill"), new Person("Jack"));
		// List persons2 = Arrays.asList(new Person("Jane"), new Person("Jim"));

		// käia üle listi elementide ja printida nimed

		for (Person person : persons1) {
			System.out.print("person1: " + person.getName() + "\t");
		}
		System.out.println();
		// for (Object person : persons2) {
		// System.out.print("person2: " + person + "\t");
		// }
		System.out.println();
	}

	@Test
	public void ex5() {
		System.out.println(fib(7));
	}

	// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144..
	// fib(7) = 8 => 5 + 8 = 13
	private int fib(int n) {
		System.out.println("fib(" + n + ")");
		if (n == 0) {
			return n;
		} else if (n == 1) {
			return n;
		} else if (n == 2) {
			return 1;
		} else
			return fib(n - 1) + fib(n - 2);
		// return 0;
	}

}
